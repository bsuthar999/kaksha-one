import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EducationEntities, EducationEntityServices } from './entities';
import { OrganizationController } from './controllers/organization/organization.controller';
import { StandardController } from './controllers/standard/standard.controller';
import { SubjectController } from './controllers/subject/subject.controller';
import { StudentController } from './controllers/student/student.controller';
import { TeacherController } from './controllers/teacher/teacher.controller';

@Module({
  imports: [TypeOrmModule.forFeature(EducationEntities)],
  providers: [...EducationEntityServices],
  exports: [...EducationEntityServices],
  controllers: [
    OrganizationController,
    StandardController,
    SubjectController,
    StudentController,
    TeacherController,
  ],
})
export class EducationModule {}
